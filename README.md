Simulation with V-REP
===

## Pre requisites

In order to run a simulated mission in V-REP the following components must be installed:

* Aerostack with all it's components (this includes V-REP necessary files)
* V-REP PRO EDU V3.6.0 , you can download V-REP from the following link (http://www.coppeliarobotics.com/downloads.html).

## Intro

In this tutorial we will execute a mission in V-REP that counts the number of QR-Codes in a V-REP world. The content of the configuration files will be explained so you can understand what's going on behind the simulation. At the end of this tutorial you will be able to run your own Aerostack missions on V-REP. **You can find all the necessary files for this tutorial at `$AEROSTACK_STACK/stack/simulation_system/vrep_simulator` and `$AEROSTACK_STACK/launchers/vrep_simulator_launchers`**

## Launching V-REP

First of all, you have to copy the V-REP ROS Interface plugin (libv_repExtRosInterface.so) into your V-REP folder and replace the existing one. This will allow the communication between V-REP embedded scripts and Aerostack throught ROS. Be aware that this is a modified version of the official plugin as we need to include custom Aerostack messages.
To launch V-REP, you should execute roscore command previously so the ROS Interface can load correctly when launching V-REP. Then, go to your V-REP installation folder, open a terminal and type `./vrep.sh`
To load the QR world click on `File -> Open Scene` at the top left corner. Select `qrworld.ttt` file. This world contains a modified version of the default quadrotor available on V-REP, including a frontal camera and all the necessary embedded scripts written in Lua to connect V-REP with Aerostack. This scripts will subscribe/publish to the necessary Aerostack topics in order to execute the mission correctly. You can visualize the content of this script by clicking in the code sheet next to Drone#0 in the scene hierarchy, but the code won't be covered in this tutorial.
Finally, you can start the simulation by clicking the play button. 

## Setting up the necessary configs files

As the mission we're going to execute makes use of some behaviors that are not in the behavior catalog by default, we should add this behavior. Open the behavior catalog in `$AEROSTACK_STACK/configs/general` and add the following lines:

```ruby
#------------------------------------------------------
# PAY_ATTENTION_TO_QR_CODE
#------------------------------------------------------
  - behavior: PAY_ATTENTION_TO_QR_CODE
    category: recurrent
    processes:
      - qr_recognizer
```

The next step is to copy the python mission to the drone 1 config files, at `$AEROSTACK_STACK/configs/drone1`. Name the mission as `mission.py` and replace the existing one.

```python
#!/usr/bin/env python2

import executive_engine_api as api
import rospy
from aerostack_msgs.msg import ListOfBeliefs

qr_codes = []

column1_points = [
  [-5.15, 1.25, 1.50],
  [-5.15, 10.65, 1.50],
  [-5.15, 10.65, 0.40],
  [-5.15, 1.25, 0.53]
]

column2_points = [
  [-6, 1.25, 0.53],
  [-5.90, 10.65, 0.53],
  [-5.90, 10.65, 1.65],
  [-5.90, 1.25, 1.5],
  [-5.90, 0, 1.5],
  [-2.35, 0, 1.5]
]

column3_points = [
  [-2.35, 1.25, 1.5],
  [-2.35, 10.65, 1.5],
  [-2.35, 10.65, 0.40],
  [-2.35, 1.25, 0.53]
]

column4_points = [
  [-3.1, 3.7, 0.53],
  [-3.1, 10.65, 0.53],
  [-3.1, 10.65, 1.65],
  [-3.1, 3.7, 1.5],
  [-3.1, 2.5, 1.5],
  [0.4, 2.5, 1.5]
]

column5_points = [
  [0.4, 3.7, 1.5],
  [0.4, 10.65, 1.5],
  [0.4, 10.65, 0.40],
  [0.4, 3.7, 0.53]
]

column6_points = [
  [-0.35, 3.7, 0.53],
  [-0.25, 10.65, 0.53],
  [-0.25, 10.65, 1.65],
  [-0.25, 3.7, 1.5],
  [-0.25, 2.5, 1.5],
  [3.25, 2.5, 1.5]
]

column7_points = [
  [3.25, 3.7, 1.5],
  [3.25, 10.65, 1.5],
  [3.25, 10.65, 0.40],
  [3.25, 3.7, 0.53]
]

column8_points = [
  [2.65, 1.15, 0.53],
  [2.65, 10.65, 0.53],
  [2.65, 10.65, 1.65],
  [2.65, 1.25, 1.5],
  [2.65, 0, 1.5],
  [6.25, 0, 1.5]
]

column9_points = [
  [6.25, 1.25, 1.5],
  [6.25, 10.65, 1.5],
  [6.25, 10.65, 0.40],
  [6.25, 1.25, 0.53]
]

column10_points = [
  [5.95, 1.25, 0.53],
  [6.05, 10.65, 0.53],
  [6.05, 10.65, 1.65],
  [6.05, 1.25, 1.5],
  [6.05, 0, 1.5]
]

points = [
  column1_points,
  column2_points,
  column3_points,
  column4_points,
  column5_points,
  column6_points,
  column7_points,
  column8_points,
  column9_points,
  column10_points
]

def qr_callback(msg):
  global qr_codes
  index = msg.beliefs.find('code(')

  if not index == -1:
    substring = msg.beliefs[index+10:index+12]  

    if not substring.isdigit():
      substring = substring[:-1]

    if qr_codes.count(substring) == 0:
      qr_codes.append(substring)
      print(substring)



def runMission():
  global qr_codes
  i = 0
  api.executeBehavior('TAKE_OFF')
  api.executeBehavior('ROTATE', angle=183)
  api.executeBehavior('GO_TO_POINT', coordinates=[-5.15, 0, 1.5])
  activated, uid = api.activateBehavior('PAY_ATTENTION_TO_QR_CODE')
  if not activated:
    raise Exception('Unable to active the QR recognizer')
  rospy.Subscriber("/drone1/all_beliefs", ListOfBeliefs, qr_callback)
  for column in points:
    for point in column:
      result = api.executeBehavior('GO_TO_POINT', coordinates=point)
      api.executeBehavior('WAIT',timeout=1) 
    api.executeBehavior('ROTATE', angle=180*(i%2))
    api.executeBehavior('WAIT',timeout=1)
    api.executeBehavior('ROTATE', angle=180*(i%2))
    i += 1
    api.executeBehavior('WAIT',timeout=1)
  print(qr_codes)
  print(len(qr_codes))
  result = api.executeBehavior('GO_TO_POINT', coordinates=[0, 0, 0.7])
  api.executeBehavior('ROTATE', angle=90)
  result = api.executeBehavior('LAND')
  print('-> result {}'.format(result))
  print('Finish mission...')
  ```
  
Basically this mission will go throught a set of points while the behavior pay_attention_to_qr_code is activated. You can keep track of the qrs in the execution viewer. 

Now, you have to execute the script located in `$AEROSTACK/launchers/vrep_simulator_launchers/qrworld.sh`. This script will launch the necessary behaviours, the execution viewer and other processes that are part of the executive layer. You should review the content of this file and make sure you understand everything. 

Now we can start the mission by typing in the terminal:

```bash
rosservice call /drone1/python_based_mission_interpreter_process/start
```

This will execute the mission we coded in python, located under `$AEROSTACK_STACK/configs/drone1/mission.py`

# ROS Topic Info

## Subscribed topics
- **/drone1/command/high_level**([droneMsgsROS/droneCommand](https://bitbucket.org/joselusl/dronemsgsros/src/master/msg/droneCommand.msg))  

## Published topics
- **/drone1/camera/front/image_raw**([sensor_msgs/Image](http://docs.ros.org/api/sensor_msgs/html/msg/Image.html))  

- **/drone1/EstimatedPose_droneGMR_wrt_GFF**([droneMsgsROS/dronePose](https://bitbucket.org/joselusl/dronemsgsros/src/master/msg/dronePose.msg))  

- **/drone1/EstimatedSpeed_droneGMR_wrt_GFF**([droneMsgsROS/droneSpeed](https://bitbucket.org/joselusl/dronemsgsros/src/master/msg/droneSpeed.msg))  

- **/drone1/estimated_pose**([droneMsgsROS/dronePose](https://bitbucket.org/joselusl/dronemsgsros/src/master/msg/dronePose.msg))  

- **/drone1/estimated_speed**([droneMsgsROS/droneSpeed](https://bitbucket.org/joselusl/dronemsgsros/src/master/msg/DroneSpeed.msg))  

- **/drone1/rotation_angles**([geometry_msgs/Vector3Stamped](http://docs.ros.org/api/geometry_msgs/html/msg/Vector3Stamped.html))  

- **/drone1/altitude**([droneMsgsROS/droneAltitude](https://bitbucket.org/joselusl/dronemsgsros/src/master/msg/droneAltitude.msg))  

- **/drone1/ground_speed**([droneMsgsROS/vector2Stamped](https://bitbucket.org/joselusl/dronemsgsros/src/master/msg/vector2Stamped.msg))  

---
