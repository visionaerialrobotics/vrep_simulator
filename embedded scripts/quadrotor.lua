if (sim_call_type == sim.syscb_init) then 
    -- Make sure we have version 2.4.13 or above (the particles are not supported otherwise)
    v = sim.getInt32Parameter(sim.intparam_program_version)
    if (v < 20413) then
        sim.displayDialog('Warning',
                         'The propeller model is only fully supported from V-REP version 2.4.13 and above.&&nThis simulation will not run as expected!',
                         sim.dlgstyle_ok, false, '', nil, {0.8,0,0,0,0,0})
    end
    
    sim.addStatusbarMessage('Starting drone simulation')

    -- Main frame
    d=sim.getObjectHandle('Quadricopter_base')

    -- Handle Track_base frame
    trackBase=sim.getObjectHandle('Track_base')
    sim.setObjectParent(trackBase, -1, true) -- we detach it from the drone frame
 
    -- Handle Propeller scripts
    --particlesAreVisible = sim.getScriptSimulationParameter(sim.handle_self, 'particlesAreVisible')
    --sim.setScriptSimulationParameter(sim.handle_tree, 'particlesAreVisible', tostring(particlesAreVisible))
    --simulateParticles = sim.getScriptSimulationParameter(sim.handle_self, 'simulateParticles')
    --sim.setScriptSimulationParameter(sim.handle_tree, 'simulateParticles', tostring(simulateParticles))

    propellerScripts={-1, -1, -1, -1}
    for i = 1,4,1 do
        propellerScripts[i] = sim.getScriptHandle('Quadricopter_propeller_respondable'..i)
    end
    heli = sim.getObjectAssociatedWithScript(sim.handle_self)

    -- Variables for control 

    taked = false -- check if the drone is taked
    take_off = false -- check if the drone is taking

    thrust_gravity = 5.33 -- compensation for gravity
    thrust         = thrust_gravity
    pitch_corr     = 0
    yaw_corr       = 0
    roll_corr      = 0
    dt             = .05 

    prop_front_left  = 1
    prop_back_left   = 2
    prop_back_right  = 3
    prop_front_right = 4
    particlesTargetVelocities = {0,0,0,0}

    twist = {0,0,0,0,0,0} -- current twist command {vx,vy,vz,-,-,rz}.
        
    pos_ini = sim.getObjectPosition(d,-1) -- get drone absolute position
    ori_ini = sim.getObjectOrientation(d, -1)
    
    pos = {0, 0, 0}

    -- elevation

    Dthrust = 1
    zref    = pos[3] -- used if z-speed command is null.
    Kzref   = 5
    Dzref   = 3

    -- yaw

    Dyaw = 1
    
    -- pitch and roll

    angle_min = .2
    angle_max = .5
    Pangle    = .3
    Dangle    = .2
    Kvcorr    = .5

    pitch_prev = 0
    roll_prev  = 0
    

    -- ROS stuff

    -- Subscribers
    high_level_sub = simROS.subscribe('/drone1/command/high_level', 'droneMsgsROS/droneCommand', 'high_level_callback')
    dAltitude_sub = -1
    dYaw_sub = -1
    pitch_roll_sub = -1
    
    -- Pubishers
    front_camera_publisher = simROS.advertise('/drone1/camera/front/image_raw', 'sensor_msgs/Image')
    simROS.publisherTreatUInt8ArrayAsString(front_camera_publisher) -- treat uint8 arrays as strings (much faster, tables/arrays are kind of slow in Lua)
    pose_publisher = simROS.advertise('/drone1/EstimatedPose_droneGMR_wrt_GFF', 'droneMsgsROS/dronePose')
    speed_publisher = simROS.advertise('/drone1/EstimatedSpeed_droneGMR_wrt_GFF', 'droneMsgsROS/droneSpeeds')
    pose_publisher2 = simROS.advertise('/drone1/estimated_pose', 'droneMsgsROS/dronePose')
    speed_publisher2 = simROS.advertise('/drone1/estimated_speed', 'droneMsgsROS/droneSpeeds')
    rotation_publisher = simROS.advertise('/drone1/rotation_angles', 'geometry_msgs/Vector3Stamped')
    altitude_publisher = simROS.advertise('/drone1/altitude', 'droneMsgsROS/droneAltitude')
    ground_speed_publisher = simROS.advertise('/drone1/ground_speed', 'droneMsgsROS/vector2Stamped')

    -- Variables
    maltitude = -1
    myaw = -1
    mpitch_roll = -1


    -- The fake shadow 
    fakeShadow=sim.getScriptSimulationParameter(sim.handle_self, 'fakeShadow')
    if (fakeShadow) then
        shadowCont=sim.addDrawingObject(sim.drawing_discpoints
                                        + sim.drawing_cyclic 
                                        + sim.drawing_25percenttransparency 
                                        + sim.drawing_50percenttransparency 
                                        + sim.drawing_itemsizes, 
                                        0.2, 0, -1, 1)
    end

    -- Prepare 2 floating views with the camera views:
    floorCam=sim.getObjectHandle('Quadricopter_floorCamera')
    frontCam=sim.getObjectHandle('FrontVision')
    floorView=sim.floatingViewAdd(0.9,0.875,0.2,0.25,14)
    --frontView=sim.floatingViewAdd(0.9,0.875,0.2,0.25,14)
    sim.adjustView(floorView,floorCam,64)
    --sim.adjustView(frontView,frontCam,64)
end 


if (sim_call_type == sim.syscb_sensing) then 

    -- Get main frame values

    pos = sim.getObjectPosition   (d, -1)
    pos[1] = pos[1] - pos_ini[1]
    pos[2] = pos[2] - pos_ini[2]
    pos[3] = pos[3] - pos_ini[3]
    ori = sim.getObjectOrientation(d, -1)
    
    -- Handling Track_base

    sim.setObjectPosition   (trackBase, -1, pos         )
    sim.setObjectOrientation(trackBase, -1, {0,0,ori[3]})

    -- compute local velocities

    local_ori         = sim.getObjectOrientation(d,trackBase)           -- orientation in the track frame
    d_global_pos, d_global_ori = sim.getObjectVelocity(d)               -- global linear and angular speeds.
    track_mat         = sim.getObjectMatrix(trackBase, -1)              -- ask me about matrices...
    track_mat_inv     = simGetInvertedMatrix(track_mat)
    track_mat_inv[ 4] = 0
    track_mat_inv[ 8] = 0
    track_mat_inv[12] = 0
    d_local_pos       = sim.multiplyVector(track_mat_inv, d_global_pos) -- linear speed in the track frame

    -- Elevation control
    
    if(twist[3] == 0) then
        -- we have to maintain current elevation at zref value
        thrust = thrust_gravity + Kzref*(zref - pos[3]) - Dzref*d_global_pos[3]
    else    
        thrust = thrust_gravity + Dthrust*(twist[3]-d_global_pos[3])
        zref   = pos[3] -- will be used in case of null z-speed command.
    end 

    -- yaw control

    dyaw        = d_global_ori[3]
    dyaw_target = twist[6]
    yaw_corr    = Dyaw*(dyaw_target - dyaw)

    -- pitch and vx control

    vx_corr      = twist[1] - d_local_pos[1]
    pitch        = local_ori[2]
    dpitch       = (pitch - pitch_prev)/dt
    pitch_prev   = pitch
    pitch_target = Kvcorr*vx_corr;
    if twist[1] >= 0 then
        if     pitch_target >  angle_max  then pitch_target =  angle_max
        elseif pitch_target < -angle_min  then pitch_target = -angle_min
        end
    elseif twist[1] < 0 then
        if     pitch_target < -angle_max  then pitch_target = -angle_max
        elseif pitch_target >  angle_min  then pitch_target =  angle_min
        end
    end
    pitch_corr   = Pangle*(pitch_target-pitch) - Dangle*dpitch

    -- roll and vy control

    vy_corr     = twist[2] - d_local_pos[2]
    roll        = -local_ori[1]
    droll       = (roll - roll_prev)/dt
    roll_prev   = roll
    roll_target = Kvcorr*vy_corr;
    if twist[2] >= 0 then
        if     roll_target >  angle_max  then roll_target =  angle_max
        elseif roll_target < -angle_min  then roll_target = -angle_min
        end
    elseif twist[2] < 0 then
        if     roll_target < -angle_max  then roll_target = -angle_max
        elseif roll_target >  angle_min  then roll_target =  angle_min
        end
    end
    roll_corr   = Pangle*(roll_target-roll) - Dangle*droll

    -- if the drone is taking and arrives to 0.7 meters it's stop
    if(take_off and pos[3]>=0.70) then
        twist[3] = 0
        take_off = false
    end


    -- ROS stuff

    -- pose message
    pose_x = pos[2]
    pose_y = -pos[1]

    own_yaw = ori[3]

    pose_msg = 
    {
        time=sim.getSystemTime(),        
        x=pos[1],
        y=pos[2],
        z=pos[3],
        yaw=own_yaw,
        pitch=ori[1],
        roll=ori[2],
        YPR_system="xYyPzR",
        target_frame="vrepDrone",
        reference_frame="vrep"
    }
    simROS.publish(pose_publisher, pose_msg)
    simROS.publish(pose_publisher2, pose_msg)

    -- speed message

    linear_vel, angular_vel = sim.getObjectVelocity(d) -- get drone velocity
    body_vel_x = math.cos(own_yaw) * linear_vel[1] - math.sin(own_yaw) * linear_vel[2]
    body_vel_y = math.sin(own_yaw) * linear_vel[1] + math.cos(own_yaw) * linear_vel[2]

    speed_msg = 
    {
            time=sim.getSystemTime(),        
            dx=linear_vel[1],
            dy=linear_vel[2],
            dz=linear_vel[3],
            dyaw=angular_vel[3],
            dpitch=angular_vel[1],
            droll=angular_vel[2]
    }
    simROS.publish(speed_publisher, speed_msg)
    simROS.publish(speed_publisher2, speed_msg)

    -- rotation message
    rotation_msg = 
    {
        vector = {
            x=ori[1] * 180 / math.pi,
            y=ori[2] * 180 / math.pi,
            z=own_yaw * 180 / math.pi 
        }            
    }
    simROS.publish(rotation_publisher, rotation_msg)

    -- drone altitude message
    drone_altitude_msg = 
    {
        altitude = pos[3],
        altitude_speed = linear_vel[3]          
    }
    simROS.publish(altitude_publisher, drone_altitude_msg)

    --groud speed message
    ground_speed_msg = 
    {
        vector = {
            x=body_vel_x,
            y=body_vel_y
        }            
    }
    simROS.publish(ground_speed_publisher, ground_speed_msg)

    -- front vision message
    local data,w,h=sim.getVisionSensorCharImage(frontCam)
    front_msg={}
    front_msg['height']=h
    front_msg['width']=w
    front_msg['encoding']='rgb8'
    front_msg['is_bigendian']=0
    front_msg['step']=w*3
    front_msg['data']=data
    simROS.publish(front_camera_publisher, front_msg)

end

-- Shutdown publishers and subscriber
if (sim_call_type == sim.syscb_cleanup) then 
    sim.removeDrawingObject  (shadowCont)
    simROS.shutdownPublisher (pose_publisher)
    simROS.shutdownPublisher (speed_publisher)
    simROS.shutdownPublisher (pose_publisher2)
    simROS.shutdownPublisher (speed_publisher2)
    simROS.shutdownPublisher (front_camera_publisher)
    simROS.shutdownPublisher (rotation_publisher)
    simROS.shutdownPublisher (altitude_publisher)
    simROS.shutdownPublisher (ground_speed_publisher)
    simROS.shutdownSubscriber(high_level_sub)

    if not (dAltitude_sub == -1) then
        simROS.shutdownSubscriber(dAltitude_sub)
    end
    if not (dYaw_sub == -1) then
        simROS.shutdownSubscriber(dYaw_sub) 
    end
    if not (pitch_roll_sub == -1) then
        simROS.shutdownSubscriber(pitch_roll_sub)
    end
end

if (sim_call_type == sim.syscb_actuation) then 

    -- Get main frame values
    s   = sim.getObjectSizeFactor(d)
    pos = sim.getObjectPosition  (d, -1)

    -- Handle the fake shadow
    if (fakeShadow) then
        itemData = {pos[1], pos[2], 0.002, 0, 0, 1, 0.2*s}
        sim.addDrawingObjectItem(shadowCont, itemData)
    end

    -- propellers velocities computation

    particlesTargetVelocities[prop_front_left ] = thrust*(1 - pitch_corr - roll_corr - yaw_corr)
    particlesTargetVelocities[prop_back_left  ] = thrust*(1 + pitch_corr - roll_corr + yaw_corr)
    particlesTargetVelocities[prop_back_right ] = thrust*(1 + pitch_corr + roll_corr - yaw_corr)
    particlesTargetVelocities[prop_front_right] = thrust*(1 - pitch_corr + roll_corr + yaw_corr)

    for i=1,4,1 do
        sim.setScriptSimulationParameter(propellerScripts[i],'particleVelocity',particlesTargetVelocities[i])
    end

end


function stopDrone()
    twist[1] = 0
    twist[2] = 0
    twist[3] = 0
    twist[4] = 0
    twist[5] = 0
    twist[6] = 0
end


-- ###################
-- #                 #
-- # Callbacks (ros) #
-- #                 #
-- ###################

function dAltitude_callback(msg)
    if (maltitude < msg.header.seq) then
        twist[3] = msg.dAltitudeCmd
        maltitude = msg.header.seq
        --print(msg.dAltitudeCmd)
    end
end

function dYaw_callback(msg)
    if (myaw < msg.header.seq) then
        if (math.abs(msg.dYawCmd) < 0.01) then
            twist[6] = 0.00198
        else
            twist[6] = -msg.dYawCmd
        end
        myaw = msg.header.seq
        --print(-msg.dYawCmd)
    end

end

function pitch_roll_callback(msg)
    if (mpitch_roll < msg.header.seq) then
        twist[1] = -msg.pitchCmd
        twist[2] = -msg.rollCmd
        mpitch_roll = msg.header.seq
        print(msg)
    end
end

function high_level_callback(msg)

    -- TAKE_OFF
    if(msg.command == 1 and not taked) then
        taked = true
        take_off = true
        if not (dAltitude_sub == -1) then
            simROS.shutdownSubscriber(dAltitude_sub)
            dAltitude_sub=-1
        end
        if not (dYaw_sub == -1) then
            simROS.shutdownSubscriber(dYaw_sub)
            dYaw_sub=-1
        end
        if not (pitch_roll_sub == -1) then
            simROS.shutdownSubscriber(pitch_roll_sub)
            pitch_roll_sub=-1
        end
        twist[3] = 0.5
        -- Avoid drone movement when is taked
        twist[2] = -0.00115
        twist[6] = 0.00198

    -- LAND
    elseif(msg.command == 3 and taked) then
        taked = false
        stopDrone()
        if not (dAltitude_sub == -1) then
            simROS.shutdownSubscriber(dAltitude_sub)
            dAltitude_sub=-1
        end
        if not (dYaw_sub == -1) then
            simROS.shutdownSubscriber(dYaw_sub)
            dYaw_sub=-1
        end
        if not (pitch_roll_sub == -1) then
            simROS.shutdownSubscriber(pitch_roll_sub)
            pitch_roll_sub=-1
        end
        twist[3] = -0.5

    -- MOVE
    elseif (msg.command == 4) then
        stopDrone()
        dAltitude_sub = simROS.subscribe('/drone1/command/dAltitude', 'droneMsgsROS/droneDAltitudeCmd', 'dAltitude_callback')
        dYaw_sub = simROS.subscribe('/drone1/command/dYaw', 'droneMsgsROS/droneDYawCmd', 'dYaw_callback')
        pitch_roll_sub = simROS.subscribe('/drone1/command/pitch_roll', 'droneMsgsROS/dronePitchRollCmd', 'pitch_roll_callback')

    -- HOVER
    elseif (msg.command == 2) then
        stopDrone()
        if not (dAltitude_sub == -1) then
            simROS.shutdownSubscriber(dAltitude_sub)
            dAltitude_sub=-1
        end
        if not (dYaw_sub == -1) then
            simROS.shutdownSubscriber(dYaw_sub)
            dYaw_sub=-1
        end 
        if not (pitch_roll_sub == -1) then
            simROS.shutdownSubscriber(pitch_roll_sub)
            pitch_roll_sub=-1
        end
        -- Avoid drone movement when is taked
        twist[2] = -0.00115
        twist[6] = 0.00198

    -- RESET and EMERGENCY STOP
    elseif(msg.command == 5) then
        stopDrone()
        taked = false
        if not (dAltitude_sub == -1) then
            simROS.shutdownSubscriber(dAltitude_sub)
            dAltitude_sub=-1
        end
        if not (dYaw_sub == -1) then
            simROS.shutdownSubscriber(dYaw_sub)
            dYaw_sub=-1
        end
        if not (pitch_roll_sub == -1) then
            simROS.shutdownSubscriber(pitch_roll_sub)
            pitch_roll_sub=-1
        end
        twist[3] = -1.5

    -- INIT
    elseif(msg.command ==6) then
        stopDrone()
        print("INIT")

    -- IDLE
    elseif(msg.command ==0) then
        stopDrone()
        print("IDLE")

    -- DESCONOCIDO
    else
        print("DESCONOCIDO")
    end
end

